#!/usr/bin/python3
# encoding=utf-8
import pymysql as pymysql

from demo.bean import userbean
from demo.config import publicconstant
import json
import pymysql.cursors



def linkDB():
    # 打开数据库连接
    db = pymysql.connect(host=publicconstant.url_host, port=publicconstant.url_port, user=publicconstant.url_user,
                         passwd=publicconstant.url_passwd, db=publicconstant.url_db, charset=publicconstant.url_charset)
    return db


# 获取表的所有数据
def getAllData(table_name):
    # 打开数据库连接
    db = linkDB()
    # 使用 cursor() 方法创建一个游标对象 cursor
    cursor = db.cursor()
    # 使用 execute()  方法执行 SQL 查询
    sql = "SELECT * FROM " + table_name
    cursor.execute(sql)
    # 获取所有记录列表
    results = cursor.fetchall()
    return results
    # 关闭数据库连接
    db.close()


# 获取表的指定字段的数据
def getAssignData(table_name, params):
    # 打开数据库连接
    db = linkDB()
    # 使用 cursor() 方法创建一个游标对象 cursor
    cursor = db.cursor()
    # 使用 execute()  方法执行 SQL 查询
    select = ""
    for x in params:
        if len(select) == 0:
            select = x
        else:
            select = select + "," + x
    if len(select) == 0:
        sql = "SELECT * FROM " + table_name
    else:
        sql = "SELECT " + select + " FROM " + table_name
    cursor.execute(sql)
    # 获取所有记录列表
    results = cursor.fetchall()
    return results
    # 关闭数据库连接
    db.close()

# 获取表的指定条件的数据
def getAssignWhereData(table_name, params,whereparams):
    # 打开数据库连接
    db = linkDB()
    # 使用 cursor() 方法创建一个游标对象 cursor
    cursor = db.cursor()
    # 使用 execute()  方法执行 SQL 查询
    select = ""
    for x in params:
        if len(select) == 0:
            select = x
        else:
            select = select + "," + x
    where = []
    for k, v in whereparams.items():
        if isinstance(v, str):
            where.append(k + ' = "' + v + '"')
        else:
            where.append(k + ' = ' + str(v))
    where_sql = ' where (' + ','.join(where) + ')'
    if len(select) == 0:
        sql = "SELECT * FROM " + table_name
    else:
        sql = "SELECT " + select + " FROM " + table_name
    if where_sql.strip():
        sql = sql + where_sql
    cursor.execute(sql)
    # 获取所有记录列表
    results = cursor.fetchall()
    return results
    # 关闭数据库连接
    db.close()


# 插入数据（单条）
def insertData(table_name, params):
    # 打开数据库连接
    db = linkDB()
    # 使用 cursor() 方法创建一个游标对象 cursor
    cursor = db.cursor()
    # SQL 插入语句
    key = []
    value = []
    for k, v in params.items():
        key.append(k)
        if isinstance(v, str):
            value.append('"' + v + '"')
        else:
            value.append(str(v))
    attrs_sql = '(' + ','.join(key) + ')'
    values_sql = ' values(' + ','.join(value) + ')'
    sql = 'insert into %s' % table_name
    sql = sql + attrs_sql + values_sql
    try:
        # 执行sql语句
        cursor.execute(sql)
        # 提交到数据库执行
        db.commit()
        return 0
    except:
        # 发生错误时回滚
        db.rollback()
        return -1
    # 关闭数据库连接
    db.close()


# 更新数据（单条）
def updateData(table_name, params, whereparams):
    # 打开数据库连接
    db = linkDB()
    # 使用 cursor() 方法创建一个游标对象 cursor
    cursor = db.cursor()
    # SQL 插入语句
    update = []
    for k, v in params.items():
        if isinstance(v, str):
            update.append(k + ' = "' + v + '"')
        else:
            update.append(k + ' = ' + str(v))
    values_sql = ' ' + ','.join(update) + ''
    where = []
    for k, v in whereparams.items():
        if isinstance(v, str):
            where.append(k + ' = "' + v + '"')
        else:
            where.append(k + ' = ' + str(v))
    where_sql = ' where (' + ','.join(where) + ')'
    sql = 'update %s' % table_name + " set "
    sql = sql + values_sql + where_sql
    try:
        # 执行sql语句
        cursor.execute(sql)
        # 提交到数据库执行
        db.commit()
        return 0
    except:
        # 发生错误时回滚
        db.rollback()
        return -1
    # 关闭数据库连接
    db.close()
