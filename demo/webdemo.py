# -*- encoding: utf-8 -*-
from flask import Flask
from flask import request
from demo.ctrl import login_ctrl
import json

app = Flask(__name__)  # 创建一个wsgi应用


@app.route('/', methods=['GET', 'POST'])
def home():
    return '欢迎来到用户系统'


@app.route('/getphonecode', methods=['GET', 'POST'])
def getphonecode():
    # 取出携带的参数
    if request.method == "POST":
        phone = request.form.get('phone')
    if request.method == "GET":
        phone = request.args.get('phone')

    result = login_ctrl.getphonecode(phone)
    return json.dumps(result, ensure_ascii=False);


@app.route('/registered', methods=['GET', 'POST'])
def registered():
    if request.method == "POST":
        name = request.form.get('name')
        if isinstance(request.form.get('age'), int):
            age = request.form.get('age')
        else:
            age = int(request.form.get('age'))
        sex = request.form.get('sex')
        addr = request.form.get('addr')
        if isinstance(request.form.get('phone'), int):
            phone = request.form.get('phone')
        else:
            phone = int(request.form.get('phone'))
        nickname = request.form.get('nickname')
        psd = request.form.get('psd')
        activation = 1
        del_type = 0
        if isinstance(request.form.get('phonecode'), int):
            phonecode = request.form.get('phonecode')
        else:
            phonecode = int(request.form.get('phonecode'))

    if request.method == "GET":
        name = request.args.get('name')
        if isinstance(request.args.get('age'), int):
            age = request.args.get('age')
        else:
            age = int(request.args.get('age'))
        sex = request.args.get('sex')
        addr = request.args.get('addr')
        if isinstance(request.args.get('phone'), int):
            phone = request.args.get('phone')
        else:
            phone = int(request.args.get('phone'))
        nickname = request.args.get('nickname')
        psd = request.args.get('psd')
        activation = 1
        del_type = 0
        if isinstance(request.args.get('phonecode'), int):
            phonecode = request.args.get('phonecode')
        else:
            phonecode = int(request.args.get('phonecode'))
    params = {}
    params["name"] = name
    params["age"] = age
    params["sex"] = sex
    params["addr"] = addr
    params["phone"] = phone
    params["nickname"] = nickname
    params["psd"] = psd
    params["activation"] = activation
    params["del_type"] = del_type
    params["phonecode"] = phonecode
    result = login_ctrl.registered_ctrl(params)
    return json.dumps(result, ensure_ascii=False);


@app.route('/login', methods=['GET', 'POST'])
def signin():
    if request.method == "POST":
        if isinstance(request.form.get('phone'), int):
            phone = request.form.get('phone')
        else:
            phone = int(request.form.get('phone'))
        psd = request.form.get('psd')

    if request.method == "GET":
        if isinstance(request.args.get('phone'), int):
            phone = request.args.get('phone')
        else:
            phone = int(request.args.get('phone'))
        psd = request.args.get('psd')
    params = {}
    params["phone"] = phone
    params["psd"] = psd
    result = login_ctrl.login(params)
    return json.dumps(result, ensure_ascii=False);


if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        port=8080,
        debug=True
    )
