from demo.db import dbctrl

import time


def getphonecode(phone):
    if len(phone) > 0:
        code_params = []
        code_params.append("phone")
        where_params = {}
        where_params["phone"] = phone
        db_select = dbctrl.getAssignWhereData("phonecode", code_params, where_params)
        if (db_select is None) or (len(db_select) == 0):
            # 没有该手机号码的的验证码发送历史添加发送验证码记录
            params = {}
            params["phone"] = phone
            params["phonecode"] = 8888
            t = time.time()
            params["time"] = int(t)
            params["type"] = 0
            sql_type = dbctrl.insertData("phonecode", params)
        else:
            # 有验证码历史记录，修改发送验证码记录信息
            params = {}
            params["phonecode"] = 8888
            t = time.time()
            params["time"] = int(t)
            params["type"] = 0
            whereparams = {}
            whereparams["phone"] = phone
            sql_type = dbctrl.updateData("phonecode", params, whereparams)
        if sql_type == 0:
            result = {"msg": "验证码发送成功", "type": 0}
            return result;
        else:
            result = {"msg": "保存验证信息失败", "type": -1}
            return result;
    else:
        result = {"msg": "验证码发送失败，请输出正确的手机号码", "type": -1}
        return result;


def registered_ctrl(params):
    code_params = []
    code_params.append("phonecode")
    where_params = {}
    where_params["phone"] = params["phone"]
    db_select = dbctrl.getAssignWhereData("phonecode", code_params, where_params)
    if (db_select is None) or (len(db_select) == 0):
        # 没有验证码记录
        result = {"msg": "注册失败", "type": -1}
        return result;
    else:
        # 从数据库根据手机号码获取验证码，取第一条记录
        for row in db_select:
            db_phonecode = row[0]
        if params["phonecode"] == db_phonecode:
            code_params = []
            code_params.append("phone")
            where_params = {}
            # 判断用户是否存在以手机号码为唯一标识
            where_params["phone"] = params["phone"]
            db_select = dbctrl.getAssignWhereData("user", code_params, where_params)
            if (db_select is None) or (len(db_select) == 0):
                user_params = {}
                user_params["name"] = params["name"]
                user_params["age"] = params["age"]
                user_params["sex"] = params["sex"]
                user_params["addr"] = params["addr"]
                user_params["phone"] = params["phone"]
                user_params["nickname"] = params["nickname"]
                user_params["psd"] = params["psd"]
                user_params["activation"] = params["activation"]
                user_params["del_type"] = params["del_type"]
                sql_type = dbctrl.insertData("user", user_params)
                if sql_type == 0:
                    result = {"msg": "注册成功", "type": 0}
                else:
                    result = {"msg": "注册失败", "type": -1}
            else:
                result = {"msg": "注册失败，该用户已注册", "type": -1}
            return result;
        else:
            result = {"msg": "注册失败,手机号码和验证码不匹配", "type": -1}
            return result;


def login(params):
    code_params = []
    code_params.append("psd")
    where_params = {}
    where_params["phone"] = params["phone"]
    db_select = dbctrl.getAssignWhereData("user", code_params, where_params)
    if (db_select is None) or (len(db_select) == 0):
        result = {"msg": "登录失败，该用户不存在", "type": -1}
        return result
    else:
        for row in db_select:
            db_psd = row[0]
        if params["psd"] == db_psd:
            result = {"msg": "登录成功", "type": 0}
            return result;
        else:
            result = {"msg": "登录失败,手机号码或密码错误", "type": -1}
            return result;
